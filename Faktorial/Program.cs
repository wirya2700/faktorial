﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faktorial
{
    class Program
    {
        static Int32 Number, Limit, Result=1;

        /// <summary>
        /// This function to calculate faktorial 
        /// </summary>
        /// <param name="Number">number to count </param>
        static void FaktorialFunction()
        {
            
            if (Limit == Number || Number - Limit == 1)
            {
                Console.Write(Number - Limit + " = " + Result);
                Console.WriteLine();
                Question();
            }else{
                Console.Write(Number - Limit + " X ");
                Result *= Number - Limit;
            }

            Limit += 1;
            FaktorialFunction();          
        }


        /// <summary>
        /// Question function for get value number from user
        /// </summary>
        static void Question()
        {
            Number = 0;
            Limit = 0;
            Result = 1;
            Console.Write("Input number : ");
            Number = Convert.ToInt32(Console.ReadLine());
            Console.Write(Number + "! = ");
            FaktorialFunction();
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            Question();
        }
    }
}
